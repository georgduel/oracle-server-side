const gulp = require('gulp');
const zip = require('gulp-zip');

gulp.task('zip', () => {
  gulp.src(['./duelServerSide/**'])
    .pipe(zip('duelServerSide.zip'))
    .pipe(gulp.dest('./dist'));
});
