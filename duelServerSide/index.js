const winston = require('winston');
const express = require('express');

const app = express();

// setup logger for local development
const logger = new (winston.Logger)({
  levels: {
    error: 0,
    warning: 1,
    info: 2,
    debug: 3,
  },
  transports: [
    new (winston.transports.Console)({ level: 'debug', colorize: true }),
  ],
});

app.use((req, res, next) => {
  if (!res.locals) {
    res.locals = {};
  }
  // res.locals.logger is pre-configured in the Oracle Commerce Cloud environment,
  // so it needs to be done manually for local development
  res.locals.logger = logger;
  next();
});

// routes get registered under /ccstorex/custom in the Oracle Commerce Cloud environment by default
app.use('/ccstorex/custom', require('./app/index'));

const port = (process.argv[2] || (process.env.npm_package_config_port || 8080));

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
