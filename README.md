# Manage the Duel server-side extension for Oracle Commerce Cloud

## Environment variables setup

Create a file named `.env` in the root directory and add the necessary variables to it:

- `OCCS_DOMAIN`: This is your shop's admin domain.
- `APP_KEY`: You have to register this as an application in your store. Go to Settings > Web APIs > Registered Applications, register a new one and copy the application key.

(There is a `example.env` that indicates how it should look like)

## Upload

- Go to the root directory, `npm install` and run `npm run upload` after that.

## Retrieve logs

Add `ADMIN_USERNAME` and `ADMIN_PASSWORD` to the `.env` file with the username and password you log in with as an admin.

To retrieve the logs run `npm run logs $level` with `$level` being one of **error, warning, debug, info**. If you don't specify a level, **error** will be the default.

To remove all logs, run `npm run logs clear`.

## Run locally

- Create `config.json` in the root directory just like `example.config.json` and put in the storefront & admin url, as well as your application key. These OCC server-side extension specific config variables are available to and needed by every extension running on the store server, so we need to provide them locally too.
- Run `npm start` and go to http://localhost:8080/ccstorex/custom/duel/products to see it working. Any errors will get logged to the console.