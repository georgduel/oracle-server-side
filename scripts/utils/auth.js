const request = require('request-promise');
require('dotenv').config();

/**
 * Authenticates with app key & resolves with the token
 * @returns {Promise<string>} Access token
 */
exports.getToken = () => (
  request.post({
    url: `${process.env.OCCS_DOMAIN}/ccadmin/v1/login`,
    form: {
      grant_type: 'client_credentials',
    },
    auth: {
      bearer: process.env.APP_KEY,
    },
  })
    .then(result => JSON.parse(result).access_token)
    .catch(err => console.error(err))
);
