const fs = require('fs');
const path = require('path');
const request = require('request-promise');
const auth = require('./utils/auth.js');
require('dotenv').config();

auth.getToken().then((token) => {
  const formData = {
    fileUpload: fs.createReadStream(path.join(__dirname, '/../dist/duelServerSide.zip')),
    filename: 'duelServerSide.zip',
    uploadType: 'extensions',
    force: 'true',
  };

  request.post({
    url: `${process.env.OCCS_DOMAIN}/ccadmin/v1/serverExtensions`,
    formData,
    auth: {
      bearer: token,
    },
  }).then((body) => {
    console.log(body);
  }).catch((err) => {
    console.error(err);
  });
});
